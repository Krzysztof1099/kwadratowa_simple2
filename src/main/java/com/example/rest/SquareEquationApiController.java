package com.example.rest;

import com.example.dto.SquareEquationDto;
import com.example.service.SquareEquationSolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/math")
public class SquareEquationApiController {
    private static final Logger LOG = LoggerFactory.getLogger(SquareEquationApiController.class);

    @GetMapping("/quadratic-function")
    public ResponseEntity<SquareEquationDto> getRoots(@RequestParam double a, @RequestParam double b, @RequestParam double c) {
        LOG.info("--- solving equation "+a+"x^2 + "+b+"x + "+c+" = 0");
        SquareEquationSolver ses = new SquareEquationSolver(a, b, c);
        return ResponseEntity.ok().body(ses.getRoots());
    }

}
