package com.example.service;

import com.example.dto.SquareEquationDto;

public class SquareEquationSolver {

    Double a, b, c, delta, x1, x2;
    SquareEquationDto roots;

    public SquareEquationDto getRoots() {
        return roots;
    }

    public SquareEquationSolver(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;
        this.delta = null;
        this.x1 = null;
        this.x2 = null;
        solver(a, b, c);
        roots = new SquareEquationDto(x1, x2);
    }

    public void solver(double a, double b, double c){
        delta = (b*b)-(4*a*c);
        if(delta>0){
            x1 = (-b+Math.sqrt(delta))/(2*a);
            x2 = (-b-Math.sqrt(delta))/(2*a);
        }else if(delta==0){
            x1 = -b/(2*a);
        }
    }

}
