package com.example.dto;

public class SquareEquationDto {
    Double x1, x2;

    public Double getX1() {
        return x1;
    }

    public Double getX2() {
        return x2;
    }

    public SquareEquationDto(Double x1, Double x2){
        this.x1 = x1;
        this.x2 = x2;
    }
}
